import React from 'react';
import './App.css';
import Home from './components/Home.jsx';
import { Routes, Route } from 'react-router-dom';
import CountryDetails from './components/CountryDetails/CountryDetails.jsx';
import Header from './components/Header/Header.jsx';
function App() {
  return (
    <><Header />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/country/:id" element={<CountryDetails />} />
      </Routes>
    </>
  )
}

export default App
