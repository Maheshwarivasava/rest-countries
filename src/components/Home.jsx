import React, { useState, useEffect } from 'react';
import Header from './Header/Header.jsx';
import Filters from './Filters/Filters.jsx';
import { useTheme } from './ThemeContext.jsx';
import datanotfoundimage from '../assets/image2.svg';
import apifetcherrorimage from '../assets/error1.svg';
import Skeletons from './Skeletons.jsx';
import 'react-loading-skeleton/dist/skeleton.css'
// import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import CountryCard from './CountryCard/CountryCard.jsx';

const DarkModeContext = React.createContext();


const Home = () => {

  const  navigate = useNavigate();
  const { darkMode } = useTheme();
  const [countries, setCountries] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [selectedRegion, setSelectedRegion] = useState('');
  const [selectedSubregion, setSelectedSubregion] = useState('');
  const [sortBy, setSortBy] = useState('population');
  const [sortOrder, setSortOrder] = useState('asc');
  const [error, setError] = useState(false);
  const [loader, setLoader] = useState(true)
  // const sortCountries = [];

  useEffect(() => {
    async function getdata() {
      try {
        let data = await fetch("https://restcountries.com/v3.1/all");
        if (!data) {
          throw new Error("Failed to fetch data")
        }
        data = await data.json();
        setCountries(data)
        setLoader(false)
      } catch (error) {
        setError(true);
      }
    }
    getdata();
  }, [])



  let filteredCountries = [...countries];
  filteredCountries = countries.filter(country => {
    return (
      country.name.common.toLowerCase().includes(searchInput.toLowerCase()) &&
      (selectedRegion === '' || country.region.toLowerCase() === selectedRegion.toLowerCase()) &&
      (selectedSubregion === '' || country.subregion.toLowerCase() === selectedSubregion.toLowerCase())
    );
  });
  const sortCountries = () => {
    const sorted = [...filteredCountries].sort((a, b) => {
      if (sortBy === 'population') {
        return sortOrder === 'asc' ? a.population - b.population : b.population - a.population;
      } else if (sortBy === 'area') {
        return sortOrder === 'asc' ? a.area - b.area : b.area - a.area;
      }
      return 0;
    });
    return sorted;
  };

  const sortedCountries = sortCountries();
  return (
    <div className={`wrapper ${darkMode ? 'dark-mode' : ''}`}>
      <div className='main-container'>


        <Filters
          searchInput={searchInput}
          setSearchInput={setSearchInput}
          selectedRegion={selectedRegion}
          setSelectedRegion={setSelectedRegion}
          selectedSubregion={selectedSubregion}
          sortedCountries={sortedCountries}
          setSelectedSubregion={setSelectedSubregion}
          sortBy={sortBy}
          setSortBy={setSortBy}
          sortOrder={sortOrder}
          setSortOrder={setSortOrder}
          countries={countries}
        />
        {error && <div className='not-found'>
          <img src={apifetcherrorimage} alt="not found" />
        </div>}
        {loader && <div className='skeleton-flex'><Skeletons loop={10} /></div>}
        {!error && filteredCountries.length === 0 && (
          <div className='not-found'>
            <img src={datanotfoundimage} alt="not found" />
          </div>

        )}
        <div className={`country-cards ${loader ? "class2" : ""} class3`} >
          {
            sortedCountries.map((country,index)=> (
            //  <CountryCard key={country.name.common} country={country} />
            <CountryCard key={index} country={country} id={country?.cca3}/>
            ))

          }
        </div>
      </div>
    </div>
  )
}

export default Home;