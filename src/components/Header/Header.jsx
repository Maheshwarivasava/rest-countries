import React from 'react';
import { useTheme } from '../ThemeContext.jsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon } from '@fortawesome/free-regular-svg-icons';
import './Header.css'

const Header = () => {
  const { darkMode, toggleDarkMode } = useTheme();

  return (
    <header className={darkMode ? 'dark-mode' : ''}>
      <h1>Where in the world?</h1>
      <button onClick={toggleDarkMode}>
        <FontAwesomeIcon icon={faMoon} className='icon-moon' />
        {darkMode ? 'Light Mode' : 'Dark Mode'}
      </button>
    </header>
  );
};

export default Header;