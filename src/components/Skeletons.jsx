import Skeleton from "react-loading-skeleton";

let Skeletons = ({ loop }) => {
  return (
    <>
      {Array(loop)
        .fill(0)
        .map((_, i) => {
          return (
            <div key={i} className="skeleton">
              <Skeleton height={"50%"} style={{ marginBottom: "0.5rem" }} duration={0.5} />
              <div className="content-skeleton">
                <Skeleton
                  duration={0.5}
                  count={4}
                  height={"1rem"}
                  width={"100%"}
                  style={{ marginBottom: "0.5rem" }}
                />
              </div>
            </div>
          );
        })}
    </>
  );
};
export default Skeletons;