import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import './Filters.css';

const Filters = ({ searchInput, setSearchInput, selectedRegion, setSelectedRegion, selectedSubregion, setSelectedSubregion, countries ,sortBy, setSortBy, sortOrder, setSortOrder}) => {
  
  const getUniqueRegions = () => {
    const regions = countries.map(country => country.region); 
    const uniqueRegions = [...new Set(regions)]; 
    return uniqueRegions;
  };
 
  const getUniqueSubregions = () => {
    const subregions = countries
      .filter(country => country.region === selectedRegion) 
      .map(country => country.subregion) 
      .filter((subregion, index, self) => self.indexOf(subregion) === index); 
    return subregions;
  };
  return (
    <div className="filters">
      <div className="search-input-container">
        <FontAwesomeIcon icon={faSearch} className="search-icon" />
        <input
          type="text"
          className="search-input"
          placeholder="Search for a country..."
          value={searchInput}
          onChange={(e) => setSearchInput(e.target.value)}
        />
      </div>
      <select
        value={selectedRegion}
        onChange={(e) => setSelectedRegion(e.target.value)}
      >
        <option value="">Filter by Region</option>
        {getUniqueRegions().map((region, index) => (
          <option key={index} value={region}>{region}</option>
        ))}
      </select>
      {selectedRegion && (
        <select
          value={selectedSubregion}
          onChange={(e) => setSelectedSubregion(e.target.value)}
        >
          <option value="">Filter by Subregion</option>
          {getUniqueSubregions().map((subregion, index) => (
            <option key={index} value={subregion}>{subregion}</option>
          ))}
        </select>
      )}
      <select
        value={sortBy}
        onChange={(e) => setSortBy(e.target.value)}
      >
        <option value="population">Sort by Population</option>
        <option value="area">Sort by Area</option>
      </select>
      <select
        value={sortOrder}
        onChange={(e) => setSortOrder(e.target.value)}
      >
        <option value="asc">Ascending</option>
        <option value="desc">Descending</option>
      </select>
    </div>
  );
};

export default Filters;
