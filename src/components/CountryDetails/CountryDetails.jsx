import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import './CountryDetails.css';
import { useTheme } from '../ThemeContext.jsx';


const DarkModeContext = React.createContext();
const CountryDetails = () => {

    const [country, setCountry] = useState();
    const { id } = useParams();
    const navigate = useNavigate();
    const { darkMode } = useTheme();

    useEffect(() => {
        const fetchCountry = async () => {
            try {
                const response = await fetch(`https://restcountries.com/v3.1/alpha/${id}`);
                if (!response) {
                    throw new Error('Failed to fetch country data');
                }
                const data = await response.json();
                setCountry(data[0]);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchCountry();
    }, [id]);
    //   console.log(country);
    return (
        <div className={`wrapper ${darkMode ? 'dark-mode' : ''}`}>
            <button onClick={() => navigate(-1)}>
                <FontAwesomeIcon icon={faArrowLeft} />Back</button>
            {country && (
                <div className="country-details">

                    <div className='flag'>
                        <img src={country.flags.svg} alt={`Flag of ${country.name.common}`} />
                    </div>
                    <div className='textData'>
                        <div className='country-info'>
                            <h1>{country.name.common}</h1>
                            <div className='detail-all'>
                                <div className='detail-part1'>
                                    <p><span>Native Name:</span> {Object.values(country?.name?.nativeName)[0].common}</p>
                                    <p><span>Population:</span> {country?.population}</p>
                                    <p><span>Region:</span> {country?.region}</p>
                                    <p><span>Sub Region:</span> {country.subregion ? country.subregion[0] : "No subregion"}</p>
                                    <p><span>Capital:</span> {country.capital ? country.capital[0] : "No Capital"}</p>
                                </div>
                                <div className='detail-part2'>
                                    <p><span>Top Level Domain:</span> {country.tld ? country.tld[0] : "No Top Level Domain"}</p>
                                    <p><span>Currencies:</span> {country?.currency}</p>
                                    <p><span>Languages: </span>{country.languages ? Object.values(country.languages).join(",") : "No Languages"}</p>
                                </div>
                            </div>
                            <div className='border'>
                                <p> <span>Border Countries: </span></p>
                                {country?.borders ? (
                                    country?.borders.map((country) => {
                                        return (
                                            <button key={country?.name?.common} className={darkMode ? "btn dark-mode" : "btn"}>
                                                {country}
                                            </button>
                                        );
                                    })
                                ) : (
                                    <p>No Border Countries</p>
                                )}

                            </div>
                        </div>
                    </div>
                </div>
            )
            }

        </div >
    );
}

export default CountryDetails;  