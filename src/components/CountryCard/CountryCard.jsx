import React from 'react';
import './CountryCard.css';
import { useNavigate } from 'react-router-dom';

const CountryCard = ({ country, id }) => {
  let navigate = useNavigate()
  let handleNavigate = () => {
    navigate(`/country/${id}`)
  }
  return (
    <div className="card" onClick={handleNavigate}>
      {/* {console.log(country)} */}
      <img src={country.flags.png} alt={country.name.common} />
      <div className="card-content">
        <h2>{country.name.common}</h2>
        <ul>
          <li><span>Population:</span> {country.population}</li>
          <li><span>Region:</span> {country.region}</li>
          <li><span>Capital:</span>{country.capital ? country.capital[0] : "No Capital"}</li>
        </ul>
      </div>
    </div>
  );
};

export default CountryCard;